from django.contrib import admin
from django.urls import path
from .views import ListCategoryView, NewCategoryView, EditCategoryView, DeleteCategoryView, ListSubCategoryView, NewSubCategoryView, EditSubCategoryView, DeleteSubCategoryView

app_name = 'inventario'
urlpatterns = [
    path('categorias/', ListCategoryView.as_view(), name='list_category'),
    path('categorias/nueva', NewCategoryView.as_view(), name='new_category'),
    path('categorias/editar/<int:pk>', EditCategoryView.as_view(), name='edit_category'),
    path('categorias/eliminar/<int:pk>', DeleteCategoryView.as_view(), name='delete_category'),
    #Sub Categorías
    path('subcategorias/', ListSubCategoryView.as_view(), name='list_subcategory'),
    path('subcategorias/nueva', NewSubCategoryView.as_view(), name='new_subcategory'),
    path('subcategorias/editar/<int:pk>', EditSubCategoryView.as_view(), name='edit_subcategory'),
    path('subcategorias/eliminar/<int:pk>', DeleteSubCategoryView.as_view(), name='delete_subcategory'),
    #Marcas
    path('marcas/', ListSubCategoryView.as_view(), name='list_brand'),
    path('marcas/nueva', NewSubCategoryView.as_view(), name='new_brand'),
    path('marcas/editar/<int:pk>', EditSubCategoryView.as_view(), name='edit_brand'),
    path('marcas/eliminar/<int:pk>', DeleteSubCategoryView.as_view(), name='delete_brand'),
    #Unidad de Medida
    path('medidas/', ListSubCategoryView.as_view(), name='list_measure'),
    path('medidas/nueva', NewSubCategoryView.as_view(), name='new_measure'),
    path('medidas/editar/<int:pk>', EditSubCategoryView.as_view(), name='edit_measure'),
    path('medidas/eliminar/<int:pk>', DeleteSubCategoryView.as_view(), name='delete_measure'),
    #Producto
    path('productos/', ListSubCategoryView.as_view(), name='list_product'),
    path('productos/nueva', NewSubCategoryView.as_view(), name='new_product'),
    path('productos/editar/<int:pk>', EditSubCategoryView.as_view(), name='edit_product'),
    path('productos/eliminar/<int:pk>', DeleteSubCategoryView.as_view(), name='delete_product'),
]