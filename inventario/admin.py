from django.contrib import admin
from inventario.models import Category, SubCategory, Marca, UnidadMedida, Product

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "code_sku", "current_existence", "last_purchase_date")
    list_filter = ["name", "current_existence", "last_purchase_date"]
    search_fields = ["name", "code_sku",]


admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Marca)
admin.site.register(UnidadMedida)
admin.site.register(Product, ProductAdmin)