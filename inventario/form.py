from django import forms
from .models import Category, SubCategory, Marca, UnidadMedida, Product


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'description', 'state']
        widget = {'description': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class SubCategoryForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.filter(state=True).order_by('name'))

    class Meta:
        model = SubCategory
        fields = ['name', 'category', 'description', 'state']
        labels = {'description': "Descripción de la subcategoría", "state": "Estado"}
        widget = {
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].empty_label = "Seleccione una categoría"
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class MarcaForm(forms.ModelForm):
    class Meta:
        model = Marca
        fields = ['name', 'description', 'state']
        labels = {'description': "Descripción de la marca", "state": "Estado"}
        widget = {'description': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})


class UnidadMedidaForm(forms.ModelForm):
    class Meta:
        model = UnidadMedida
        fields = ['name', 'description', 'state']
        labels = {'description': "Descripción de la unidad de medida", "state": "Estado"}
        widget = {'description': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})