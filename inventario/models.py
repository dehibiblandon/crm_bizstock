from django.db import models
from transversal.models import BaseManage
# Create your models here.

class Category(BaseManage):
    description = models.CharField(
        max_length=150,
        help_text="Descripción de la categoría",
        null=True,
        blank= True,
    )
    def __str__(self):
        return '{}'.format(self.name)
    class Meta:
        verbose_name_plural = "Categorias"

class SubCategory(BaseManage):
    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL, null=True,
        verbose_name="Categoría Asociada",
        default=None
    )
    description = models.CharField(
        max_length=150,
        verbose_name="Descripción de la Sub Categoría",
        null=True,
        blank= True,
    )
    def __str__(self):
        return '{} - {}'.format(self.category.name, self.name)
    
    class Meta:
        verbose_name_plural = "Subcategorias"

class Marca(BaseManage):
    description = models.CharField(
        max_length=100, 
        verbose_name ='Descripción de la marca', 
        unique=True,
        null=True,
        blank= True,
        )

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Marcas"

class UnidadMedida(BaseManage):
    description = models.CharField(
        max_length=100, 
        verbose_name ='Descripción de la unidad de medida', 
        unique=True,
        null=True,
        blank= True,
    )

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Unidades de medida"


class Product(BaseManage):
    code_sku = models.CharField(
        max_length=20, 
        unique=True,
        verbose_name ="Código Sku",
    )
    bar_code = models.CharField(
        max_length=50,
        verbose_name="Código de Barras",
    )
    description = models.CharField(
        max_length=200, 
        verbose_name ='Descripción del Producto', 
        unique=True,
        null=True,
        blank= True,
    )
    price = models.PositiveIntegerField(
        default=0,
        verbose_name ='Precio Unitario del Producto',
        null=False,
        blank= False,
    )
    current_existence = models.IntegerField(
        default=0,
        verbose_name = "Existencia en Inventarios",
        null=False,
        blank= False,
    )
    last_purchase_date = models.DateField(
        verbose_name= "Fecha de última Compra",
        null=True, 
        blank=True,
    )
    brand = models.ForeignKey(
        Marca, on_delete=models.SET_NULL, null=True,
        verbose_name="Marca Asociada",
        default=None
    )
    unit_measure = models.ForeignKey(
        UnidadMedida, on_delete=models.SET_NULL, null=True,
        verbose_name="Unidad de Medida Asociada",
     )
    sub_category = models.ForeignKey(
        SubCategory, on_delete=models.SET_NULL, null=True,
        verbose_name="Sub Categoría Asociada",
        )
    #privider

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = "Productos"