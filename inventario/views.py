from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.views import generic
from django.urls import reverse_lazy
from .models import Category, SubCategory, Marca, UnidadMedida, Product
from .form import CategoryForm, SubCategoryForm

# Create your views here.

class ListCategoryView(generic.ListView):
    model = Category
    template_name = "category/category_list.html"
    context_object_name = "categories"
    ordering = ['id']

class NewCategoryView(SuccessMessageMixin, generic.CreateView):
    model = Category
    template_name = "category/category_new.html"
    context_object_name = "categoria"
    form_class = CategoryForm
    success_url = reverse_lazy ('inventario:list_category')
    success_message = "Categoríoa creada satisfactoriamente"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_created = self.request.user.username

        return super().form_valid(form)

class EditCategoryView(SuccessMessageMixin, generic.UpdateView):
    model = Category
    template_name = "category/category_new.html"
    context_object_name = "categoria"
    form_class = CategoryForm
    success_url = reverse_lazy ('inventario:list_category')
    success_message = "Categoría ctualizada"

    def form_valid(self, form):
        form.instance.user_created = self.request.user.username

        return super().form_valid(form)

class DeleteCategoryView(SuccessMessageMixin, generic.DeleteView):
    model = Category
    template_name = "category/category_delete.html"
    success_url = reverse_lazy("inventario:list_category")
    success_message = "La categoría ha sido eliminada correctamente"
#SUB CATEGORIAS
class ListSubCategoryView(generic.ListView):
    model = SubCategory
    template_name = "sub_category/sub_category_list.html"
    context_object_name = "subcategories"
    ordering = ['id']

class NewSubCategoryView(SuccessMessageMixin, generic.CreateView):
    model = SubCategory
    template_name = "sub_category/sub_category_new.html"
    context_object_name = "subcategoria"
    form_class = SubCategoryForm
    success_url = reverse_lazy ('inventario:list_subcategory')
    success_message = "Categoríoa creada satisfactoriamente"

    # agregamos el usuario que realiza la modificación
    def form_valid(self, form):
        form.instance.user_created = self.request.user.username

        return super().form_valid(form)

class EditSubCategoryView(SuccessMessageMixin, generic.UpdateView):
    model = SubCategory
    template_name = "sub_category/sub_category_new.html"
    context_object_name = "subcategoria"
    form_class = SubCategoryForm
    success_url = reverse_lazy ('inventario:list_subcategory')
    success_message = "Categoría ctualizada"

    def form_valid(self, form):
        form.instance.user_created = self.request.user.username

        return super().form_valid(form)

class DeleteSubCategoryView(SuccessMessageMixin, generic.DeleteView):
    model = SubCategory
    template_name = "sub_category/sub_category_delete.html"
    success_url = reverse_lazy("inventario:list_subcategory")
    success_message = "La categoría ha sido eliminada correctamente"

#brand

class ListBrandView(generic.ListView):
    model = Marca
    template_name = "brand/brand_list.html"
    context_object_name = "brand"
    ordering = ['id']

# class NewCategoryView(SuccessMessageMixin, generic.CreateView):
#     model = Category
#     template_name = "category_new.html"
#     context_object_name = "categoria"
#     form_class = CategoryForm
#     success_url = reverse_lazy ('inventario:list_category')
#     success_message = "Categoríoa creada satisfactoriamente"

#     # agregamos el usuario que realiza la modificación
#     def form_valid(self, form):
#         form.instance.user_created = self.request.user.username

#         return super().form_valid(form)

# class EditCategoryView(SuccessMessageMixin, generic.UpdateView):
#     model = Category
#     template_name = "category_new.html"
#     context_object_name = "categoria"
#     form_class = CategoryForm
#     success_url = reverse_lazy ('inventario:list_category')
#     success_message = "Categoría ctualizada"

#     def form_valid(self, form):
#         form.instance.user_created = self.request.user.username

#         return super().form_valid(form)

# class DeleteCategoryView(SuccessMessageMixin, generic.DeleteView):
#     model = Category
#     template_name = "category_delete.html"
#     success_url = reverse_lazy("inventario:list_category")
#     success_message = "La categoría ha sido eliminada correctamente"