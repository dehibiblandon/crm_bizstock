from django.db import models
from transversal.models import BaseManage
# Create your models here.

class TypeIdentification(BaseManage):
    class Meta:
        verbose_name = "Tipo de documento"
        verbose_name_plural = "Tipos de documentos"


class Sector(BaseManage):
    class Meta:
        verbose_name = "Sector"
        verbose_name_plural = "Sectores"

class Provider(BaseManage):
    sector = models.ForeignKey(
        Sector,
        on_delete=models.CASCADE,
        verbose_name="Sector",
        blank=False,
        null=False,
    )
    type_identification = models.ForeignKey(
        TypeIdentification,
        on_delete=models.CASCADE,
        verbose_name="Tipo Identificación",
        blank=False,
        null=False,
    )
    identification_number = models.CharField(
        verbose_name="Número de Identificación",
        max_length=255,
        blank=False,
        null=False,
        default=''
    )
    mail = models.EmailField(
        verbose_name="Email",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    phone = models.CharField(
        verbose_name="Teléfono",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    whatsapp = models.CharField(
        verbose_name="Contacto Whastapp",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    address = models.CharField(
        verbose_name="Dirección",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )

    class Meta:
        verbose_name = "Proveedor"
        verbose_name_plural = "Proveedor"