from django.contrib import admin
from provider.models import TypeIdentification, Sector, Provider

# Register your models here.
class ProviderAdmin(admin.ModelAdmin):
    list_display = ("name", "sector" )
    list_filter = ["name", "sector"]
    search_fields = ["name", "identification_number",]

admin.site.register(TypeIdentification)
admin.site.register(Sector)
admin.site.register(Provider, ProviderAdmin)
