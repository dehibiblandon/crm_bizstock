from django.contrib import admin
from django.urls import path
from transversal.views import CustomLoginView, CustomLogoutView
app_name = 'transversal'
urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', CustomLogoutView.as_view(), name='logout'),
]