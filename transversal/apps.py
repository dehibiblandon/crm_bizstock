from django.apps import AppConfig


class TransversalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transversal'
