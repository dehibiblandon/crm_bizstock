from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models.signals import pre_save
from django.dispatch import receiver


# Create your models here.
class BaseManage(models.Model):
    name = models.CharField(
        verbose_name="Nombre",
        max_length=255,
        blank=False,
        null=False,
        default=''
    )
    created_date = models.DateTimeField(
        verbose_name="Publicación",
        default=timezone.now,
    )
    modified_date = models.DateTimeField(
        verbose_name="Actualización",
        default=timezone.now,
    )
    user_modified =  models.CharField(
        max_length=150,
        verbose_name="Usuario que Crea o Modifica",
        blank=True,
        null=True
    )
    class Meta:
        abstract = True

    def __str__(self):
        return self.name




