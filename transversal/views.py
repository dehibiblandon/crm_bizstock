from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView
from django.urls import reverse_lazy

from django.contrib.auth.views import PasswordChangeView
from django.conf import settings

class CustomLoginView(LoginView):
    template_name = 'sign-in.html'

    # def get_success_url(self):
    #     user = self.request.user

    #     # Verificamos si el usuario es superadministrador o staff
    #     if user.is_superuser or user.is_staff:
    #         return reverse_lazy('admin:index')          
    #     # Verificar si el usuario pertenece a los grupos especificados
    #     elif any(group in settings.RESTRICTED_ROLES for group in user.groups.values_list('name', flat=True)):
    #         return reverse_lazy('aforo_ooh_booking')
    #     return reverse_lazy('login') 

class CustomLogoutView(LogoutView):
    next_page = reverse_lazy('transversal:login')