from django.db import models
from transversal.models import BaseManage
from provider.models import Provider
from inventario.models import Product
import datetime

# Create your models here.
class Purchase(BaseManage):
    date_buy =  models.DateField(
        verbose_name="Fecha de compra",
        blank=False,
        null=False,
        default=datetime.date.today
    )
    observation = models.TextField(
        verbose_name="Observación de la Compra",
        blank=True,
        null=True,
        default='',
        max_length=300,
    )
    num_invoice = models.CharField(
        verbose_name="Factura de venta",
        max_length=50,
        blank=False,
        null=False,
        default="",
    )
    date_invoice = models.DateField(
        verbose_name="Fecha de Factura",
        blank=False,
        null=False,
        default=datetime.date.today
    )
    sub_total = models.FloatField(        
        verbose_name="Sub Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    discount = models.FloatField(        
        verbose_name="Descuento",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    total = models.FloatField(        
        verbose_name="Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )

    def __str__(self):
        return '{}'.format(self.observation)

    def save(self):
        self.total = self.sub_total - self.discount

        super(Purchase, self).save()

    class Meta:
        verbose_name_plural = "Compras"
        verbose_name = "Compra"


class ItemPurchase(BaseManage):
    purchase = models.ForeignKey(
        Purchase, on_delete=models.SET_NULL, null=True,
        verbose_name="Compra Asociada",
        default=None
    )
    provider = models.ForeignKey(
        Provider, on_delete=models.SET_NULL, null=True,
        verbose_name="Proveedor Asociado",
        default=None
    )
    amount = models.PositiveIntegerField(
        default=0,
        verbose_name="Cantidad comprada",
        blank=False,
        null=False,
    )
    price = models.FloatField(        
        verbose_name="Precio",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    sub_total = models.FloatField(        
        verbose_name="Sub Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    discount = models.FloatField(        
        verbose_name="Descuento",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    total = models.FloatField(        
        verbose_name="Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    cost = models.FloatField(        
        verbose_name="Costo",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )

    def __str__(self):
        return '{}'.format(self.name)

    def save(self):
        self.subTotal = float(float(int(self.amount)) * float(self.price))
        self.total = self.sub_total - float(self.discount)

        super(ItemPurchase, self).save()

    class Meta:
        verbose_name_plural = "Detalle compras"
        verbose_name = "Detalle compra"

class OrderPurchase(BaseManage):
    date_order = models.DateField(
        verbose_name="Fecha de Orden",
        blank=False,
        null=False,
        default=datetime.date.today
    )
    description = models.CharField(
        max_length=200,
        verbose_name='Descripción Orden',
        null=True,
        blank= True,
    )
    sub_total_item = models.FloatField(        
        verbose_name="Sub Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    provider = models.ForeignKey(
        Provider, on_delete=models.SET_NULL,
        null= True,
        verbose_name="Proveedor Asociado",
        default=''
    )

class ItemOrderPurchase(BaseManage):
    product = models.ForeignKey(
        Product, on_delete=models.SET_NULL,
        null=True,
        verbose_name= "Producto / Artículo"
    )
    order = models.ForeignKey(
        OrderPurchase, on_delete=models.SET_NULL,
        null=True,
        verbose_name= "Orden Asociada"
    )
    amount = models.PositiveIntegerField(
        default=0,
        verbose_name="Cantidad comprada",
        blank=False,
        null=False,
    )
    price_item = models.FloatField(
        verbose_name="Precio Artículo",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    total_item = models.FloatField(        
        verbose_name="Precio Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    def __str__(self):
        return '{}'.format(self.name)

    def save(self):
        self.total_item = float(float(int(self.amount)) * float(self.price_item))

        super(ItemOrderPurchase, self).save()

    class Meta:
        verbose_name_plural = "Detalle Orden"
        verbose_name = "Detalle Orden"