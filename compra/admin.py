from django.contrib import admin
from compra.models import Purchase, ItemPurchase

# Register your models here.
class PurchaseAdmin(admin.ModelAdmin):
    list_display = ("date_buy", "num_invoice", "proveedor" )
    list_filter = ["date_buy", "proveedor"]
    search_fields = ["date_buy", "proveedor",]

class ItemPurchaseAdmin(admin.ModelAdmin):
    list_display = ("compra", "provider" )
    list_filter = ["compra", "provider"]
    search_fields = ["provider"]

admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(ItemPurchase, ItemPurchaseAdmin)