from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.db.models import Sum
from transversal.models import BaseManage
from inventario.models import Product
from django.utils import timezone

# Create your models here.

class Customer(BaseManage):
    TIPOCLIENTE = [
        ('Natural', 'Natural'),
        ('Juridica', 'Juridica'),
        ]
    first_name = models.CharField(
        verbose_name="Nombre Cliente",
        max_length=100,
        blank=False,
        null=False,
    )
    last_name = models.CharField(
        verbose_name="Apellido Cliente",
        max_length=100,
        blank=False,
        null=False,
    )
    identification_number = models.CharField(
        verbose_name="Número de Identificación",
        max_length=255,
        blank=False,
        null=False,
        default=''
    )
    mail = models.EmailField(
        verbose_name="Email",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    phone = models.CharField(
        verbose_name="Teléfono",
        max_length=255,
        blank=True,
        null=True,
        default=''
    )
    type_client = models.CharField(
        max_length=10, 
        verbose_name= "Tipo de cliente",
        choices=TIPOCLIENTE,
        blank=False,
        null=False, 
        default='Natural'
    )

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    class Meta:
        verbose_name_plural = "Clientes"


class Invoice(BaseManage):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.SET_NULL, null=True,
        verbose_name="Cliente Factura",
        default=None
    )
    date_generated = models.DateTimeField(
        verbose_name="Fecha Generación Factura",
        default=timezone.now,
        )
    sub_total = models.FloatField(        
        verbose_name="Sub Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    discount = models.FloatField(        
        verbose_name="Descuento",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    total = models.FloatField(        
        verbose_name="Total",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )

    def __str__(self):
        return '{}'.format(self.id)

    def save(self):
        self.total = self.sub_total - self.discount

        super(Invoice, self).save()

    class Meta:
        verbose_name_plural = "Facturas"
        verbose_name = "Factura"

class ItemsInvoice(BaseManage):
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.SET_NULL, null=True,
        verbose_name="Factura Asociada",
        default=None
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.SET_NULL, null=True,
        verbose_name="Producto Asociada",
        default=None
    )
    amount = models.BigIntegerField(
        verbose_name="Cantidad Artículos",
        blank=False,
        null=False,
        default=0
        )
    price = models.FloatField(        
        verbose_name="Precio Item",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    sub_total_item = models.FloatField(        
        verbose_name="Subtotal Item",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    discuount_item = models.FloatField(        
        verbose_name="Descuento Item",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )
    total_item = models.FloatField(        
        verbose_name="Total Item",
        max_length=255,
        blank=False,
        null=False,
        default=0
    )

    def __str__(self):
        return '{}'.format(self.product)

    def save(self):
        self.sub_total_item = float(float(int(self.amount)) * float(self.price))
        self.total_item = self.sub_total_item - float(self.discuount_item)

        super(ItemsInvoice, self).save()

    class Meta:
        verbose_name_plural = "Detalle facturas"
        verbose_name = "Detalle factura"
