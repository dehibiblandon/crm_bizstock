from django.contrib import admin
from facturacion.models import Customer, Invoice, ItemsInvoice

# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name" )
    list_filter = ["identification_number"]
    search_fields = ["name", "identification_number",]

class InvoiceAdmin(admin.ModelAdmin):
    list_display = ("id", "date_generated", "customer", "total" )
    list_filter = ["date_generated"]
    search_fields = ["id", "date_generated",]

class ItemsInvoiceAdmin(admin.ModelAdmin):
    list_display = ("invoice", "product", "amount" )
    list_filter = ["invoice", "product"]
    search_fields = ["id", "invoice", "product"]

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(ItemsInvoice, ItemsInvoiceAdmin)
